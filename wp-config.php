<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'weill_local');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');



define('WP_HOME','http://michael-largue.dev/');
define('WP_SITEURL','http://michael-largue.dev/');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '_z0VMDL>/8f&Yy6n>Tkw[N_*=28+BVM!bk #<Tp)|{dFC6Nf)}u$Y#6nOQp-wMB<');
define('SECURE_AUTH_KEY',  '+0b^grcJ{)mvt@(*>,w6l6G[W$fxD_9T;|y?f/+~Fq]gpHX88@=!g.k9g*njwMGv');
define('LOGGED_IN_KEY',    '*VEM!Q5mNh:/OFzjxa6s4E5[]@Tw?qgn8/t{:C+G,nuH_bz]WgDx9+-f#:`h0_9X');
define('NONCE_KEY',        '{V8e`Jqe2`L~8&(y2cJk.@qnw<JFr@~942r(|b1Jp%=-;X[-!>(~gf$+J<{XR6|-');
define('AUTH_SALT',        '0IA9F3S-RyUT-7zAfKj|K@$qs.FlM$88Re>,nXr/I+b?$|yxYW-}$)h:<p&704uD');
define('SECURE_AUTH_SALT', 'iI@E6GCq{H;KErpC<,%VehBg4bQh}tqW*@LtaArP[wcn+Ys6b?!$C>0g5#0dP@4r');
define('LOGGED_IN_SALT',   'azQ%ttEt%gLXNN=Vf_Os@MwK[+Lf>u_3kl;Hu.<u%(-:%|k}1YI|5_xAVn8P@P}G');
define('NONCE_SALT',       'b[>w:%@>:9xo38cEzKM}ry+q~>5F7Eq2k3/kVp,)BQp;7~*C<z9ds{j<ap(6cP~i');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
