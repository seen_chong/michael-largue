			<footer class="site-footer">
			  <div class="footer-links">
			  		<?php wp_nav_menu( array( 'theme_location' => 'footer-menu' ) ); ?>

			  </div>
			  <div class="footer-text">
			  	<ul>
			  		<li>&copy; 2016 Geoffrey Weill Associates, Inc.</li>
			  		<li>29 Broadway #2205</li>
			  		<li>New York, NY 10006</li>
			  		<li>212.288.1144</li>
			  		<li>​news@geoffreyweill.com</li>
			  	</ul>
			  </div>
			</footer>

		<?php wp_footer(); ?>

		<!-- analytics -->

	</body>
</html>

<script type="text/javascript">
jQuery(".open-sesame").click(function(){

    jQuery("ul#menu-header-menu ").slideToggle("easeOutBounce");
});
</script>