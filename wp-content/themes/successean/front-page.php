<?php get_header('alt'); ?>


  
	<div class="landing-slider">
		<?php masterslider(1); ?>
		<div class="landing-text">
			<!-- <h5><?php the_field('top_quote'); ?></h5> -->
			<!-- <img class="arrow-down" src="<?php echo get_template_directory_uri(); ?>/img/arrow-down.png" rel="shortcut icon"> -->
			<!-- <span class="attributed-to"><?php the_field('attributed_to'); ?></span> -->
		</div>
	</div>

	<div class="second-level">
		<div class="main-wrapper">	
			<div class="opening-text">
				<h2>GEOFFREY WEILL ASSOCIATES</h2>
				<h6>PUBLIC RELATIONS AND MARKETING IN TRAVEL & TOURISM</h6>
				<p><?php the_field('opening_text'); ?></p>
				<button><a href="/who-we-are/">Let's Get Started</a></button>
			</div>
			
		</div>
	</div>

	<div class="our-services-block">
		<div class="our-services-title">
			<img src="<?php echo get_template_directory_uri(); ?>/img/funny-shape.png" rel="shortcut icon"> 
			<h2>Our <br /> Services</h2>
		</div>
		<div class="our-services-button">
			<button><a href="/what-we-do/">View All</a></button>
		</div>
	</div>

	<div class="our-services-grid">
		<div class="serv-grid" style="background:url('')">
			<img class="serv-img" src="<?php the_field('marketing_image'); ?>">
			<div class="serv-grid-text">
				<div class="open-grid">
					<h3>Social Media</h3>
					<p><?php the_field('marketing'); ?></p>
				</div>
				
			</div>
			

		</div>
		<div class="serv-grid">
			<img class="serv-img" src="<?php the_field('brochures_&_collateral_image'); ?>">
			<div class="serv-grid-text">
				<div class="open-grid">
					<h3>Brochures, Collateral</h3>
					<p><?php the_field('brochures_&_collateral'); ?>.</p>
				</div>
				
			</div>
			
		</div>
		<div class="serv-grid">
			<img class="serv-img" src="<?php the_field('classic_public_relations_image'); ?>">
			<div class="serv-grid-text">
				<div class="open-grid">
					<h3>Classic Public Relations</h3>
					<p><?php the_field('classic_public_relations'); ?></p>
				</div>

			</div>
			
		</div>
		<div class="serv-grid">
			<img class="serv-img" src="<?php the_field('internet_image'); ?>">
			<div class="serv-grid-text">
				<div class="open-grid">
					<h3>The Web</h3>
					<p><?php the_field('internet'); ?></p>
				</div>

			</div>
			
		</div>
		<div class="serv-grid">
			<img class="serv-img" src="<?php the_field('special_events_image'); ?>">
			<div class="serv-grid-text">
				<div class="open-grid">
					<h3>Special Events</h3>
					<p><?php the_field('special_events'); ?></p>
				</div>

			</div>
			
		</div>
		<div class="serv-grid">
			<img class="serv-img" src="<?php the_field('branding_image'); ?>">
			<div class="serv-grid-text">
				<div class="open-grid">
					<h3>Branding</h3>
					<p><?php the_field('branding'); ?></p>
				</div>

			</div>
			
		</div>
	</div>

<!-- 	<div class="browse-clients">
		<div class="weills-world">
			<h3>Weill's World</h3>
			<img src="<?php echo get_template_directory_uri(); ?>/img/pinponts_alt.png">
			<button><a href="/clients">Browse Our Clients</a></button>
		</div>
	</div> -->
      
</div>

<!-- end wrapper -->

<?php get_footer(); ?>


<script type="text/javascript">
jQuery(".serv-grid").hover(function(e){

    jQuery("p", this).slideToggle("easeOutBounce");
    jQuery("#.serv-grid-text:after").rotateRight(45);
    return false;
});
</script>
