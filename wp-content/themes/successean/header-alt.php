<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>

		<link href="//www.google-analytics.com" rel="dns-prefetch">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.ico" rel="shortcut icon">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed">

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="<?php bloginfo('description'); ?>">

		<?php wp_head(); ?>
		<script>
        // conditionizr.com
        // configure environment tests
        conditionizr.config({
            assets: '<?php echo get_template_directory_uri(); ?>',
            tests: {}
        });
        </script>

	</head>
	<body <?php body_class(); ?>>

		<!-- wrapper -->
		<div class="page-wrap">


			<header>

				<nav class="weill-nav-alt" role="navigation">
					<ul>
						<li class="weill-top-level-alt">

							<a href="https://www.facebook.com/pages/GEOFFEY-WEILL-ASSOCIATES/185336415555" target="blank">
								<img class="social-menu-icon" src="<?php echo get_template_directory_uri(); ?>/img/social-fb.png" rel="shortcut icon">
							</a>

							<a href="https://twitter.com/weillaway" target="blank">
								<img class="social-menu-icon" src="<?php echo get_template_directory_uri(); ?>/img/social-twitter.png" rel="shortcut icon">
							</a>
							<a href="https://www.instagram.com/weill.away/" target="blank">
								<img class="social-menu-icon" src="<?php echo get_template_directory_uri(); ?>/img/social-ig.png" rel="shortcut icon">
							</a>

							<a class="open-sesame">
								<img class="burger-menu" src="<?php echo get_template_directory_uri(); ?>/img/interface.png" rel="shortcut icon">
							</a>

						</li>

						<?php wp_nav_menu( array( 'theme_location' => 'header-menu' ) ); ?>
					</ul>
					<h1><?php the_title(); ?></h1>

<!-- 					<a href="/">
						<img class="new-logo" src="<?php echo get_template_directory_uri(); ?>/img/new-logo.jpg" rel="shortcut icon">
					</a> -->
				</nav>

			</header>

