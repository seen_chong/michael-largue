<?php get_header('alt'); ?>


  
	<div class="accolades-hero" style="background-image:url(<?php echo get_template_directory_uri(); ?>/img/accolades-hero.png);">

		<div class="column-blocks">
			<div class="column-block-top">
				<img class="column-top-acc" src="<?php echo get_template_directory_uri(); ?>/img/wwa-column-top-1.png">
				<div class="member-quote">
					<p>“As the youngest child of a pioneer travel writer, I was the one who got to stay home and get the dolls and snow globes from far-flung places. When I turned fifteen, I tossed the dolls and set out on my own. I’m still trying to make up for the first 15 years at home…”</p>
					<img src="<?php echo get_template_directory_uri(); ?>/img/insignia.png">
					<h6>Ann-Rebecca Laschever <br />
						<span class="member-position">Senior Vice President</span>
					</h6>
					<img class="featured-on" src="<?php echo get_template_directory_uri(); ?>/img/featured-on.jpg">
					<button><a href="/contact-us">Get in touch with us</a></button>
				</div>
			</div>


			<div class="column-block-bottom">
				<img class="column-bottom" src="<?php echo get_template_directory_uri(); ?>/img/wwa-column-bottom-1.png">
				<div class="member-test">
					<h5>Testimonials</h5>
					<p>"When we started working with Geoffrey Weill, the public and travel trade professionals were not acquainted with our hotels and not even knowledgeable about Iguazu Falls. This situation started to change by the first year after we started to work with WEILL. Their professionalism and knowledge became quickly apparent; when we attended trade shows, people knew who we are. The press trips WEILL has organized always included top journalists who have produced important pieces in well known publications."</p>
				</div>
			</div>
		</div>


	</div>

	<div class="wwa-second-level">


		<?php echo do_shortcode('[gallery size="large" ids="38,37,36,35,34"]'); ?>

	</div>


      
</div>
<!-- end wrapper -->


<style type="text/css">
	ul#menu-header-menu {
		display: none;
	}
	.weill-nav h6 {
		display: none;
	}
</style>
<?php get_footer(); ?>
