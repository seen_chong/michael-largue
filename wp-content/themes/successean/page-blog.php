<?php get_header('alt'); ?>


  
	<div class="accolades-hero" style="background-image:url(<?php echo get_template_directory_uri(); ?>/img/accolades-hero.png);">

		<div class="column-blocks">
			<img class="logo-col-block" src="<?php echo get_template_directory_uri(); ?>/img/weill-logo-1.png">
			<div class="column-block-top">
				<img class="column-archive" src="<?php echo get_template_directory_uri(); ?>/img/wwa-column-top-1.png">

				<div class="blog-widgets">

					<?php
						if(is_active_sidebar('blog-widget')){
							dynamic_sidebar('blog-widget');
						}
					?>
						
				</div>
			</div>
		</div>


	</div>

	<div class="blog-block">
		<div class="blog-posts">

					<?php
			  $args = array(
			    'post_type' => 'blog-posts'
			    );
			  $products = new WP_Query( $args );
			  if( $products->have_posts() ) {
			    while( $products->have_posts() ) {
			      $products->the_post();
			?>

			<div class="blog-post">
				<h2><?php the_field('title'); ?></h2>
				<img src="<?php the_field('image'); ?>">
				<p><?php the_field('post'); ?></p>
			</div>

			<?php
		    		}
		  		}
			  else {
			    echo 'No Employees Found';
			  }
		  	?>
		</div>
	</div>


      
</div>
<!-- end wrapper -->


<style type="text/css">
	ul#menu-header-menu {
		display: none;
	}
	.weill-nav h6 {
		display: none;
	}
</style>
<?php get_footer(); ?>
