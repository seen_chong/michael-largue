<?php get_header('alt'); ?>

	<div class="our-clients-grid">

		<?php
		  $args = array(
		    'post_type' => 'client',
		    'posts_per_page' => 99 
		    );
		  $products = new WP_Query( $args );
		  if( $products->have_posts() ) {
		    while( $products->have_posts() ) {
		      $products->the_post();
		?>


		<div class="client-grid">
			<img class="client-grid-img" src="<?php the_field('image'); ?>">
				<div class="client-grid-overlay">
					<a href="<?php the_permalink(); ?>">
						<h3><?php the_field('name'); ?></h3>
						<h4><?php the_field('location'); ?></h4>
					</a>
				</div>
			
		</div>

		<?php
	    		}
	  		}
		  else {
		    echo 'No Clients  Found';
		  }
	  	?>

	</div>

</div> <!-- .page-wrap -->

<?php get_footer(); ?>
