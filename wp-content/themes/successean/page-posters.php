<?php get_header('alt'); ?>

	<div class="posters">

		<div class="poster-wrapper">


		<div class="poster-gallery">
			<?php echo do_shortcode("[huge_it_gallery id='1']"); ?>
		</div>
		<div class="poster-side">
			<div class="poster-side-header">
				<h2>Why Poster Gallery?</h2>
				<h4>Says Geoffrey Weill...</h4>
				<img src="<?php echo get_template_directory_uri(); ?>/img/insignia1.png">

			</div>
			<div class="poster-side-text">
				<p>I began collecting travel posters in the 1970's. But my love affair with the graphics of travel began much earlier.</p>
				<p>When I was six, my family and I vacationed in France.For the trip, I was given a small brown cardboardsuitcase to hold my favorite toy cars and crayons. One morning in a hotel lobby in Reims, the concierge walked solemnly over to me and dropped into a crouch. From behind his back he produced a luggage label to which a giant tongue gave a giant lick, and then he pasted the label, askew of course, onto the side of my little brown suitcase.</p>
				<p>When I was six, my family and I vacationed in France.
				For the trip, I was given a small brown cardboard
				suitcase to hold my favorite toy cars and crayons. One
				morning in a hotel lobby in Reims, the concierge
				walked solemnly over to me and dropped into a crouch.
				From behind his back he produced a luggage label to
				which a giant tongue gave a giant lick, and then he
				pasted the label, askew of course, onto the side of my
				little brown suitcase.</p>
				<p>The label seemed to have enormous portent. My little
				brown cardboard suitcase was no longer a mere
				receptacle for toys: it had been deemed "luggage." And
				I was no longer a small boy toting cars and crayons. I
				was a "traveler."</p>
			</div>
		</div>
		</div>
	</div>

</div> <!-- .page-wrap -->

<?php get_footer(); ?>
