<?php get_header('alt'); ?>

	<div class="three-col-layout">
		<div class="col-service col-layout">
			<div class="col-serv-list">
				<h3>All Services</h3>
				<ul>

						<?php
						  $args = array(
						    'post_type' => 'services'
						    );
						  $products = new WP_Query( $args );
						  if( $products->have_posts() ) {
						    while( $products->have_posts() ) {
						      $products->the_post();
						?>

					<li>
						<a class="link" href="#" data-rel="content<?php the_field('id'); ?>"><?php the_field('service'); ?></a>
					</li>

						<?php
					    		}
					  		}
						  else {
						    echo 'No Services Found';
						  }
					  	?>
				</ul>
			</div>
		</div>


		<div class="col-focal col-layout" id="bg" style="background-image:url(<?php echo get_template_directory_uri(); ?>/img/HOMEDHS.jpg")>
			<div class="overlay-container" id="search-container">

				<!-- <div class="overlay-bg"></div> -->

				<div class="content-container">

					<div class="col-focal-overlay-1">
						<p>Click the service on the left to find out more of what we do.</p>
					</div>

						<?php
						  $args = array(
						    'post_type' => 'services'
						    );
						  $products = new WP_Query( $args );
						  if( $products->have_posts() ) {
						    while( $products->have_posts() ) {
						      $products->the_post();
						?>

					<div class="col-focal-overlay" id="content<?php the_field('id'); ?>" style="display:none; background-image:url(<?php the_field('background'); ?>">
						<h2><?php the_field('service'); ?></h2>
						<p><?php the_field('text'); ?></p>
					</div>
						<?php
					    		}
					  		}
						  else {
						    echo 'No Services Found';
						  }
					  	?>
				</div>
			</div>
<!-- 			<div class="ns">
				<h4>Next Service <br /> <span class="next-title">Classic Public Relations</span></h4>
			</div> -->
			
		</div>






		<div class="col-test col-layout">
			<div class="quote-block">
				<q>Most nights, after I've turned and plumped and flattened my pillow to find that ideally cooling patch of linen, I recite mantras of routes flown or ships sailed or hotels adored in order to lull myself to sleep.</q>
				<p>Geoffrey Weill, <span class="tan">President</span></p>
			</div>

			<div class="test-block">
				<h3>Testimonials</h3>

				<div class="test-quote-block"> 
					<q>Geoffrey is not only one of the most fun PR people I have ever worked with (andthat includes a lot more PR people than I care to recall), he actually has this rare resource called integrity. I wish him continued success in his life and work!</q>
				<p>Leslia Shasha, <span class="tan">Iguazu Grand Hotel</span></p>
				</div>

			</div>
			
		</div>
	</div>

</div> <!-- .page-wrap -->



<style type="text/css">

/*#bg, .overlay-bg{
  background-image: url('/wp-content/themes/successean/img/asset-1.jpg');
  background-repeat: no-repeat;
  background-size: 100% auto;
}*/
/*#bg {
  background-position: center top;
  padding: 70px 90px 120px 90px;
}*/

#search-container {
  position: relative;
}


@media (max-width: 600px ) {
  #bg { padding: 10px; }
  .overlay-bg { background-position: center -10px; }
}

#search h2, #search h5, #search h5 a { text-align: center; color: #fefefe; font-weight: normal; }
</style>


<?php get_footer(); ?>

<script type="text/javascript">
jQuery(".link").click(function(e) {
    e.preventDefault();
    jQuery('.content-container div').hide();
    jQuery('#' + jQuery(this).data('rel')).show();
});
</script>
