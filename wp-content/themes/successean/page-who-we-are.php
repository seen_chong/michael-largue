<?php get_header('alt'); ?>


  
	<div class="wwa-hero" style="background-image:url(<?php the_field('main_image'); ?>);">

		<div class="column-blocks">
			


			<div class="column-block-top">
				<img class="column-top" src="<?php echo get_template_directory_uri(); ?>/img/wwa-column-top.png">
								<div class="member-roll">
					<h5><a class="link" href="#" data-rel="about">About Us</a></h5>
					<ul>

						<?php
						  $args = array(
						    'post_type' => 'company'
						    );
						  $products = new WP_Query( $args );
						  if( $products->have_posts() ) {
						    while( $products->have_posts() ) {
						      $products->the_post();
						?>

						<li>
							<a class="link" href="#" data-rel="employee<?php the_field('employee_id'); ?>">
							<?php the_field('name'); ?>, <span class="tan"><?php the_field('position'); ?></span>
							</a>
						</li>

						<?php
					    		}
					  		}
						  else {
						    echo 'No Employees Found';
						  }
					  	?>
					</ul>
				</div>
			</div>
			<div class="column-block-bottom">
				<img class="column-bottom" src="<?php echo get_template_directory_uri(); ?>/img/wwa-column-bottom.png">

				<div class="member-roll">
					<ul>
						<li>We Specialize in one field...
							<p>...and one field only: tourism, travel and hospitality. Because word gets around that we do a good job, we're periodically invited to bid for accounts in other fields. We always say “no thanks,” because it's the tourism and hospitality business that we know inside-out, and that's where our strengths lie.</p>
						</li>
						<li>Now in our 3rd Decade...</li>
						<li>We're Never Reluctant...</li>
						<li>We Help...</li>
						<li>The Media Trusts Us...</li>
						<li>We hate Shlock...</li>
						<li>And We're Picky too...</li>
					</ul>

				</div>

			</div>
		</div>


	<div class="content-container">
		<div class="hero-wwa-text" id="about">
			<h3>About Us</h3>
			<h5>Known simply as Weill, we're a full-service public relations, marketing and communications agency specializing in the fields of tourism, hospitality and travel. Our clients are government tourist boards, hotels, airlines, tour sponsors and tour operators.</h5>
			<p>
				<span class="intro-text">Members of the Weill Team</span>
				come from a variety of backgrounds. We're multilinguel. And multinational. We combine a transatlantic sensibility with all-American talent and flair. We've worked with and for government tourist offices, tour operators, airlines, hotel chains, travel agencies, non-profit organizations, public relations specialists, advertising agencies, marketing experts, research specialists, graphic designers, printers, writers, journalists, authors, magazines, newspapers, filmmakers, impresarios, and with all levels of print and velectronic media.
			</p>
			<img class="weill-logo-2" src="<?php echo get_template_directory_uri(); ?>/img/weill-logo-2.png">
		</div>


			<?php
			  $args = array(
			    'post_type' => 'company'
			    );
			  $products = new WP_Query( $args );
			  if( $products->have_posts() ) {
			    while( $products->have_posts() ) {
			      $products->the_post();
			?>

		<div class="hero-wwa-text" id="employee<?php the_field('employee_id'); ?>" style="display:none;">
			<h3><?php the_field('name'); ?></h3>
			<img src="<?php the_field('image'); ?>" align=left style="padding-right:12px">
			<p>
				<i><?php the_field('bio_intro'); ?></i>
			</p>
			<p><?php the_field('bio'); ?></p>
		</div>

			<?php
		    		}
		  		}
			  else {
			    echo 'No Bio Found';
			  }
		  	?>

	</div>

	</div>

	<div class="wwa-second-level">
		<div class="wwa-second-level-text">
			<p><span class="second-level-intro">Founded in 1995, WEILL</span> has grown from a one-room-one-client-1.5 staff company, to a noted, award-winning firm with multiple clients and a continually expanding staff of specialists in tourism communications.</p>

			<p><span class="second-level-intro">"What sets us apart,"</span> says company president, Geoffrey Weill, "is that this company was and continues to be run and staffed, by people who, like me, simply enjoy writing about travel,about hotels, about the romance of taking a vacation. We’re this eclectic grouping of individuals who can think of few things more pleasurable than waxing lyrical about the perfect hotel or the most divine meal...people who get a kick out of enthusing others about travel, and who, of course, are passionate about traveling themselves."</p>
		</div>	
	</div>


</div>




      
<!-- </div> -->

<!-- end wrapper -->


<style type="text/css">
	.weill-nav h6 {
		display: none;
	}
</style>
<?php get_footer(); ?>

<script type="text/javascript">
jQuery(".link").click(function(e) {
    e.preventDefault();
    jQuery('.content-container div').hide();
    jQuery('#' + jQuery(this).data('rel')).show();
});
</script>
