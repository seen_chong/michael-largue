<?php get_header('alt'); ?>

	<div class="single-client-grid">

		<div class="client-grid">
			<img class="client-grid-img" src="<?php the_field('image'); ?>">
			<div class="client-grid-overlay">
				<h3><?php the_field('name'); ?></h3>
				<h4><?php the_field('location'); ?></h4>
			</div>
		</div>

		<div class="client-hero">
			<img src="<?php the_field('hero_image'); ?>">
		</div>
	</div>

	<div class="client-main">
		<div class="wrap">
			<img class="client-logo" src="<?php the_field('logo'); ?>">
			<p><?php the_field('testimonial'); ?></p>

			<!-- <img src="<//?php echo get_template_directory_uri(); ?>/img/insignia.png"> -->
			<h6><?php the_field('client_name'); ?> <br />
				<span class="member-position"><?php the_field('client_position'); ?></span>
			</h6>
		</div>
	</div>

	<div class="client-text">
		<div class="wrap">
			<p><?php the_field('main_text'); ?></p>
		</div>
	</div>
	
	<div class="prev-next-clients">
		<div class="wrap">

			<div class="prev-client">
				<span>Previous Client</span> <br />
				<?php next_post_link('%link'); ?>
			</div>

			<div class="next-client">
				<span>Next Client</span> <br />
				<?php previous_post_link('%link'); ?>
			</div>

		</div>
	</div>

</div> <!-- .page-wrap -->

<?php get_footer(); ?>
